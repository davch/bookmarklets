#### **readable**

````javascript
function copyText(x){  //x is this.id
  var range = document.createRange();
  var el = document.getElementById(x);
  range.selectNodeContents(el);
  var sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
  var status, color, content, check;   //store all values;
  if(document.execCommand('copy')){
    status = "Copied"; color = "green";
  } else{
    status = "Copy Failed"; color = "red";
  }

  var tip = document.createElement("b");
  var pwTipClass = pwTipClass || "pwTipClass";
  tip.classList.add(pwTipClass, color);
  
  setTimeout(function(){
    tip.parentNode.removeChild(tip);
  },3000);
  el.appendChild(tip); return false;
}

function removeDiv(x){ //x array of three ids
  var el = document.getElementById(x);
    if(el){el.parentNode.removeChild(el);}
    else {console.log(x[i]+" not found!");}
  return false;
}

function removeNewLine(x){x = x.replace(/(\r\n|\n|\r)/igm, "");return x;}

var inputs = document.getElementsByTagName("input"); //get all input elements
if(inputs.length<1) throw new Error("No Inputs"); //if no input boxes on page
var pwBoxClass = "pwBoxClass";
var pwTagId = "pwTag";//if the custom tag is already there, throw error
if(document.getElementById(pwTagId)) throw new Error("Close existing Div!");

//create elements & set IDs
//custom html tag to group all three elems in ove, to ease removal at close
var pwTag = document.createElement("pw-Tag"); pwTag.id = pwTagId;
var div = document.createElement("div"); div.id="pwBoxDiv";
var style = document.createElement("style");
var scr = document.createElement("script");

//get functions' contents as string
scr.text += copyText.toString();
scr.text += removeDiv.toString();

//create the x button
var btnClose = document.createElement("a"); btnClose.id="pwBoxBtnClose";

btnClose.innerHTML = "&times;";
btnClose.setAttribute("onclick", removeDiv.name+'("'+pwTag.id+'");');
btnClose.title = "Close";
div.appendChild(btnClose);

var pw = 0; //set password found flag
var str ="";
for(var i=0; i<inputs.length; i++){
  if (inputs[i].type.toLowerCase() == "password"){
    pw++; //count
    str += '<span class="'+pwBoxClass+'" id="id' + i + '" contenteditable="true" spellcheck="false" title="Click to Copy" onclick="'+copyText.name+'(this.id);">' + inputs[i].value + "</span>";
    }
  }
if(!pw) throw new Error("No PWs");

//set the password box div contents
var title = '<b title="by DavChana.com">Password';
if(pw>1) title += "s";

title += ":</b>"

var css = cssTemp = "";
div.innerHTML += title+str;

css = '#' + div.id;
//define all css rules with no space in front;
css += removeNewLine(`{
position:fixed;
display:flex;
flex-direction:column;
z-index:9999;
padding:10px 20px 10px 10px;
background:#fff;
box-shadow: 0px 0px 10px 2px grey;
font:14px/20px monospace;
box-sizing:border-box;
max-width:300px;
min-width:200px;
top:5px;
left:calc(-50vw + 50%);
right:calc(-50vw + 50%);
margin-left:auto;
margin-right:auto;
}`);

css += '#' + btnClose.id;
css += removeNewLine(`{
position:absolute;
top:5px;
right:5px;
padding:0;
font:20px sans-serif;
color:#000;
cursor:pointer;
text-decoration:none;
transition: all 0.2s linear;
}`);

css += '#' + btnClose.id + ':hover';
css += removeNewLine(`{
  color: red;
  transform: scale(1.5) rotate(90deg);
}`);

css += '.' + pwBoxClass + ':hover,.' + pwBoxClass + ':focus';
css += `{background:pink;}`;


css += '.' + pwBoxClass;
css += removeNewLine(`{
margin-bottom:1px;
padding:2px;
cursor:copy;
}`);

var pwTipClass = "pwTipClass";
var greenClass = "green";
var redClass = "red";

css += '.' + pwTipClass;
css += removeNewLine(`{
font-size:1.5em;
padding-left:3px;
position:absolute;
opacity:0;
animation:fadeInOut 3s linear 0s;
}
@keyframes fadeInOut{
0%,100%{
opacity:0;
}10%,75%{
opacity:1;
}
}`);

css += '.' + greenClass + '::after{content:"✓";color:' + greenClass + ';}';
css += '.' + redClass + '::after{content: "✗";color:' + redClass + ';}';
style.innerHTML = css;

//inject all three elements in document
pwTag.appendChild(scr);
pwTag.appendChild(style);
pwTag.appendChild(div);
//console.log(pwBox);
document.body.appendChild(pwTag);
````

#### minified prettyPrint
````javascript
function a(b) {
  var c = document.createRange();
  b = document.getElementById(b);
  c.selectNodeContents(b);
  var r = window.getSelection();
  r.removeAllRanges();
  r.addRange(c);
  c = document.execCommand("copy") ? "green" : "red";
  var l = document.createElement("b");
  l.classList.add("pwTipClass", c);
  setTimeout(function() {
    l.parentNode.removeChild(l);
  }, 3000);
  b.appendChild(l);
  return !1;
}
function d(b) {
  var c = document.getElementById(b);
  c ? c.parentNode.removeChild(c) : console.log(b[e] + " not found!");
  return !1;
}
function f(b) {
  return b = b.replace(/(\r\n|\n|\r)/igm, "");
}
var g = document.getElementsByTagName("input");
if (1 > g.length) {
  throw Error("No Inputs");
}
if (document.getElementById("pwTag")) {
  throw Error("Close existing Div!");
}
var h = document.createElement("pw-Tag");
h.id = "pwTag";
var k = document.createElement("div");
k.id = "pwBoxDiv";
var m = document.createElement("style"), n = document.createElement("script");
n.text += a.toString();
n.text += d.toString();
var p = document.createElement("a");
p.id = "pwBoxBtnClose";
p.innerHTML = "&times;";
p.setAttribute("onclick", d.name + '("' + h.id + '");');
p.title = "Close";
k.appendChild(p);
for (var q = 0, t = "", e = 0; e < g.length; e++) {
  "password" == g[e].type.toLowerCase() && (q++, t += '<span class="pwBoxClass" id="id' + e + '" contenteditable="true" spellcheck="false" title="Click to Copy" onclick="' + a.name + '(this.id);">' + g[e].value + "</span>");
}
if (!q) {
  throw Error("No PWs");
}
var u = '<b title="by DavChana.com">Password';
1 < q && (u += "s");
u += ":</b>";
var v = cssTemp = "";
k.innerHTML += u + t;
v = "#" + k.id;
v += f("{\nposition:fixed;\ndisplay:flex;\nflex-direction:column;\nz-index:9999;\npadding:10px 20px 10px 10px;\nbackground:#fff;\nbox-shadow: 0px 0px 10px 2px grey;\nfont:14px/20px monospace;\nbox-sizing:border-box;\nmax-width:300px;\nmin-width:200px;\ntop:5px;\nleft:calc(-50vw + 50%);\nright:calc(-50vw + 50%);\nmargin-left:auto;\nmargin-right:auto;\n}");
v += "#" + p.id;
v += f("{\nposition:absolute;\ntop:5px;\nright:5px;\npadding:0;\nfont:20px sans-serif;\ncolor:#000;\ncursor:pointer;\ntext-decoration:none;\ntransition: all 0.2s linear;\n}");
v += "#" + p.id + ":hover";
v += f("{\n  color: red;\n  transform: scale(1.5) rotate(90deg);\n}");
v += ".pwBoxClass:hover,.pwBoxClass:focus";
v += "{background:pink;}";
v += ".pwBoxClass";
v += f("{\nmargin-bottom:1px;\npadding:2px;\ncursor:copy;\n}");
v += ".pwTipClass";
v += f("{\nfont-size:1.5em;\npadding-left:3px;\nposition:absolute;\nopacity:0;\nanimation:fadeInOut 3s linear 0s;\n}\n@keyframes fadeInOut{\n0%,100%{\nopacity:0;\n}10%,75%{\nopacity:1;\n}\n}");
v += '.green::after{content:"\u2713";color:green;}';
v += '.red::after{content: "\u2717";color:red;}';
m.innerHTML = v;
h.appendChild(n);
h.appendChild(m);
h.appendChild(k);
document.body.appendChild(h);
````

#### minified
````javascript
function a(b){var c=document.createRange();b=document.getElementById(b);c.selectNodeContents(b);var r=window.getSelection();r.removeAllRanges();r.addRange(c);c=document.execCommand("copy")?"green":"red";var l=document.createElement("b");l.classList.add("pwTipClass",c);setTimeout(function(){l.parentNode.removeChild(l)},3E3);b.appendChild(l);return!1}function d(b){var c=document.getElementById(b);c?c.parentNode.removeChild(c):console.log(b[e]+" not found!");return!1}function f(b){return b=b.replace(/(\r\n|\n|\r)/igm,"")}var g=document.getElementsByTagName("input");if(1>g.length)throw Error("No Inputs");if(document.getElementById("pwTag"))throw Error("Close existing Div!");var h=document.createElement("pw-Tag");h.id="pwTag";var k=document.createElement("div");k.id="pwBoxDiv";var m=document.createElement("style"),n=document.createElement("script");n.text+=a.toString();n.text+=d.toString();var p=document.createElement("a");p.id="pwBoxBtnClose";p.innerHTML="&times;";p.setAttribute("onclick",d.name+'("'+h.id+'");');p.title="Close";k.appendChild(p);for(var q=0,t="",e=0;e<g.length;e++)"password"==g[e].type.toLowerCase()&&(q++,t+='<span class="pwBoxClass" id="id'+e+'" contenteditable="true" spellcheck="false" title="Click to Copy" onclick="'+a.name+'(this.id);">'+g[e].value+"</span>");if(!q)throw Error("No PWs");var u='<b title="by DavChana.com">Password';1<q&&(u+="s");u+=":</b>";var v=cssTemp="";k.innerHTML+=u+t;v="#"+k.id;v+=f("{\nposition:fixed;\ndisplay:flex;\nflex-direction:column;\nz-index:9999;\npadding:10px 20px 10px 10px;\nbackground:#fff;\nbox-shadow: 0px 0px 10px 2px grey;\nfont:14px/20px monospace;\nbox-sizing:border-box;\nmax-width:300px;\nmin-width:200px;\ntop:5px;\nleft:calc(-50vw + 50%);\nright:calc(-50vw + 50%);\nmargin-left:auto;\nmargin-right:auto;\n}");v+="#"+p.id;v+=f("{\nposition:absolute;\ntop:5px;\nright:5px;\npadding:0;\nfont:20px sans-serif;\ncolor:#000;\ncursor:pointer;\ntext-decoration:none;\ntransition: all 0.2s linear;\n}");v+="#"+p.id+":hover";v+=f("{\n  color: red;\n  transform: scale(1.5) rotate(90deg);\n}");v+=".pwBoxClass:hover,.pwBoxClass:focus";v+="{background:pink;}";v+=".pwBoxClass";v+=f("{\nmargin-bottom:1px;\npadding:2px;\ncursor:copy;\n}");v+=".pwTipClass";v+=f("{\nfont-size:1.5em;\npadding-left:3px;\nposition:absolute;\nopacity:0;\nanimation:fadeInOut 3s linear 0s;\n}\n@keyframes fadeInOut{\n0%,100%{\nopacity:0;\n}10%,75%{\nopacity:1;\n}\n}");v+='.green::after{content:"\u2713";color:green;}';v+='.red::after{content: "\u2717";color:red;}';m.innerHTML=v;h.appendChild(n);h.appendChild(m);h.appendChild(k);document.body.appendChild(h);
````
#### bookmarkleted
````javascript
javascript:(function()%7Bfunction%20a(b)%7Bvar%20c%3Ddocument.createRange()%3Bb%3Ddocument.getElementById(b)%3Bc.selectNodeContents(b)%3Bvar%20r%3Dwindow.getSelection()%3Br.removeAllRanges()%3Br.addRange(c)%3Bc%3Ddocument.execCommand(%22copy%22)%3F%22green%22%3A%22red%22%3Bvar%20l%3Ddocument.createElement(%22b%22)%3Bl.classList.add(%22pwTipClass%22%2Cc)%3BsetTimeout(function()%7Bl.parentNode.removeChild(l)%7D%2C3E3)%3Bb.appendChild(l)%3Breturn!1%7Dfunction%20d(b)%7Bvar%20c%3Ddocument.getElementById(b)%3Bc%3Fc.parentNode.removeChild(c)%3Aconsole.log(b%5Be%5D%2B%22%20not%20found!%22)%3Breturn!1%7Dfunction%20f(b)%7Breturn%20b%3Db.replace(%2F(%5Cr%5Cn%7C%5Cn%7C%5Cr)%2Figm%2C%22%22)%7Dvar%20g%3Ddocument.getElementsByTagName(%22input%22)%3Bif(1%3Eg.length)throw%20Error(%22No%20Inputs%22)%3Bif(document.getElementById(%22pwTag%22))throw%20Error(%22Close%20existing%20Div!%22)%3Bvar%20h%3Ddocument.createElement(%22pw-Tag%22)%3Bh.id%3D%22pwTag%22%3Bvar%20k%3Ddocument.createElement(%22div%22)%3Bk.id%3D%22pwBoxDiv%22%3Bvar%20m%3Ddocument.createElement(%22style%22)%2Cn%3Ddocument.createElement(%22script%22)%3Bn.text%2B%3Da.toString()%3Bn.text%2B%3Dd.toString()%3Bvar%20p%3Ddocument.createElement(%22a%22)%3Bp.id%3D%22pwBoxBtnClose%22%3Bp.innerHTML%3D%22%26times%3B%22%3Bp.setAttribute(%22onclick%22%2Cd.name%2B'(%22'%2Bh.id%2B'%22)%3B')%3Bp.title%3D%22Close%22%3Bk.appendChild(p)%3Bfor(var%20q%3D0%2Ct%3D%22%22%2Ce%3D0%3Be%3Cg.length%3Be%2B%2B)%22password%22%3D%3Dg%5Be%5D.type.toLowerCase()%26%26(q%2B%2B%2Ct%2B%3D'%3Cspan%20class%3D%22pwBoxClass%22%20id%3D%22id'%2Be%2B'%22%20contenteditable%3D%22true%22%20spellcheck%3D%22false%22%20title%3D%22Click%20to%20Copy%22%20onclick%3D%22'%2Ba.name%2B'(this.id)%3B%22%3E'%2Bg%5Be%5D.value%2B%22%3C%2Fspan%3E%22)%3Bif(!q)throw%20Error(%22No%20PWs%22)%3Bvar%20u%3D'%3Cb%20title%3D%22by%20DavChana.com%22%3EPassword'%3B1%3Cq%26%26(u%2B%3D%22s%22)%3Bu%2B%3D%22%3A%3C%2Fb%3E%22%3Bvar%20v%3DcssTemp%3D%22%22%3Bk.innerHTML%2B%3Du%2Bt%3Bv%3D%22%23%22%2Bk.id%3Bv%2B%3Df(%22%7B%5Cnposition%3Afixed%3B%5Cndisplay%3Aflex%3B%5Cnflex-direction%3Acolumn%3B%5Cnz-index%3A9999%3B%5Cnpadding%3A10px%2020px%2010px%2010px%3B%5Cnbackground%3A%23fff%3B%5Cnbox-shadow%3A%200px%200px%2010px%202px%20grey%3B%5Cnfont%3A14px%2F20px%20monospace%3B%5Cnbox-sizing%3Aborder-box%3B%5Cnmax-width%3A300px%3B%5Cnmin-width%3A200px%3B%5Cntop%3A5px%3B%5Cnleft%3Acalc(-50vw%20%2B%2050%25)%3B%5Cnright%3Acalc(-50vw%20%2B%2050%25)%3B%5Cnmargin-left%3Aauto%3B%5Cnmargin-right%3Aauto%3B%5Cn%7D%22)%3Bv%2B%3D%22%23%22%2Bp.id%3Bv%2B%3Df(%22%7B%5Cnposition%3Aabsolute%3B%5Cntop%3A5px%3B%5Cnright%3A5px%3B%5Cnpadding%3A0%3B%5Cnfont%3A20px%20sans-serif%3B%5Cncolor%3A%23000%3B%5Cncursor%3Apointer%3B%5Cntext-decoration%3Anone%3B%5Cntransition%3A%20all%200.2s%20linear%3B%5Cn%7D%22)%3Bv%2B%3D%22%23%22%2Bp.id%2B%22%3Ahover%22%3Bv%2B%3Df(%22%7B%5Cn%20%20color%3A%20red%3B%5Cn%20%20transform%3A%20scale(1.5)%20rotate(90deg)%3B%5Cn%7D%22)%3Bv%2B%3D%22.pwBoxClass%3Ahover%2C.pwBoxClass%3Afocus%22%3Bv%2B%3D%22%7Bbackground%3Apink%3B%7D%22%3Bv%2B%3D%22.pwBoxClass%22%3Bv%2B%3Df(%22%7B%5Cnmargin-bottom%3A1px%3B%5Cnpadding%3A2px%3B%5Cncursor%3Acopy%3B%5Cn%7D%22)%3Bv%2B%3D%22.pwTipClass%22%3Bv%2B%3Df(%22%7B%5Cnfont-size%3A1.5em%3B%5Cnpadding-left%3A3px%3B%5Cnposition%3Aabsolute%3B%5Cnopacity%3A0%3B%5Cnanimation%3AfadeInOut%203s%20linear%200s%3B%5Cn%7D%5Cn%40keyframes%20fadeInOut%7B%5Cn0%25%2C100%25%7B%5Cnopacity%3A0%3B%5Cn%7D10%25%2C75%25%7B%5Cnopacity%3A1%3B%5Cn%7D%5Cn%7D%22)%3Bv%2B%3D'.green%3A%3Aafter%7Bcontent%3A%22%5Cu2713%22%3Bcolor%3Agreen%3B%7D'%3Bv%2B%3D'.red%3A%3Aafter%7Bcontent%3A%20%22%5Cu2717%22%3Bcolor%3Ared%3B%7D'%3Bm.innerHTML%3Dv%3Bh.appendChild(n)%3Bh.appendChild(m)%3Bh.appendChild(k)%3Bdocument.body.appendChild(h)%7D)()
````