[*Edit*](https://gitlab.com/davch/bookmarklets/edit/master/README.md) | [*Share as HTML*](https://gitlab.com/davch/bookmarklets/blob/master/README.md)

----

### Contents

* []()
* []()
* []()
* []()
* []()
* []()
* []()

---

#### Send Prompt to whatsmydns.com

* **Name:** `WMyDNS`

* **readable**
````javascript
var str = prompt("NS,foo.bar").split(",");
window.location.href="https://www.whatsmydns.net/#" + str[0].toUpperCase() + "/" + str[1];
````

* **minified**
````javascript
var a=prompt("NS,foo.bar").split(",");window.location.href="https://www.whatsmydns.net/#"+a[0].toUpperCase()+"/"+a[1];
````

* **bookmarkleted**
````javascript
javascript:(function()%7Bvar%20a%3Dprompt(%22NS%2Cfoo.bar%22).split(%22%2C%22)%3Bwindow.location.href%3D%22https%3A%2F%2Fwww.whatsmydns.net%2F%23%22%2Ba%5B0%5D.toUpperCase()%2B%22%2F%22%2Ba%5B1%5D%7D)()
````

* **JSBIN:** https://jsbin.com/futuvuc/4/edit?js,output


#### What is my Screen Resolution? 

* **Name:** WMyScreenResolution

* **readable**
````javascript
alert("This Screen's Resolution is " + screen.width + " x " + screen.height);
````

* **minified**
````javascript
//same as readable
alert("This Screen's Resolution is " + screen.width + " x " + screen.height);
````

* **bookmarkleted**
````javascript
javascript:(function()%7Balert(%22This%20Screen's%20Resolution%20is%20%22%20%2B%20screen.width%20%2B%20%22%20x%20%22%20%2B%20screen.height)%7D)()
````

* **JSBIN:** https://jsbin.com/raqove/edit?js,console,output
 
#### Check if Domain has G Suite?

* **Name:** HasGSUite?
 
2017-08-21

* **readable**
````javascript
var domain = prompt("Has G Suite?"); if(domain) window.location.href="https://admin.google.com/"+domain+"/VerifyAdminAccountPasswordReset";
````

* **minified**
````javascript
var a=prompt("Has G Suite?");a&&(window.location.href="https://admin.google.com/"+a+"/VerifyAdminAccountPasswordReset"); //advanced
````

* **bookmarkleted**
````javascript
javascript:(function()%7Bvar%20a%3Dprompt(%22Has%20G%20Suite%3F%22)%3Ba%26%26(window.location.href%3D%22https%3A%2F%2Fadmin.google.com%2F%22%2Ba%2B%22%2FVerifyAdminAccountPasswordReset%22)%7D)()
````

* **JSBIN:** 

#### Do not Display any URLs in @<i></i>print media CSS

* **Name:** NoPrintURL

* **readable**
````javascript
var head = document.head, style = document.createElement('style');
style.type = 'text/css';
style.appendChild(document.createTextNode('@media print{a[href]:after{content:none}}'));
head.appendChild(style);
````

* **minified**
````javascript
var a=document.head,b=document.createElement("style");b.type="text/css";b.appendChild(document.createTextNode("@media print{a[href]:after{content:none}}"));a.appendChild(b);
````

* **bookmarkleted**
````javascript
javascript:(function()%7Bvar%20a%3Ddocument.head%2Cb%3Ddocument.createElement(%22style%22)%3Bb.type%3D%22text%2Fcss%22%3Bb.appendChild(document.createTextNode(%22%40media%20print%7Ba%5Bhref%5D%3Aafter%7Bcontent%3Anone%7D%7D%22))%3Ba.appendChild(b)%7D)()
````

* **JSBIN:** https://jsbin.com/vonebey/edit?html,js,console

#### View Passwords (& copy)

* **Name:** viewPasswords  
* https://gitlab.com/davch/bookmarklets/blob/master/viewPasswords.md

* **readable**  
https://gitlab.com/davch/bookmarklets/edit/master/viewPasswords.md#-readable

* **minified prettyPrint**  
https://gitlab.com/davch/bookmarklets/edit/master/viewPasswords.md#minified-prettyprint

* **minified**
https://gitlab.com/davch/bookmarklets/edit/master/viewPasswords.md#minified

* **bookmarkleted**  
https://gitlab.com/davch/bookmarklets/blob/master/viewPasswords.md#bookmarkleted

#### Get Exchange Rates by asking data in prompt box & redirecting to xe.com constructed URL

* **Name:** xe.com

* **readable**
````javascript
function foo(text=false){
  var str = "USD QAR 10.09";
  var help = "";
  if(!text) help = "";
    else help = str;
  text = text || str;
  var seed = prompt(text, help);
  //console.log(seed);
  if(seed == null){
    throw new Error ("Cancelled!");
  } else if(seed == ""){
    foo("No Data!! Example:");
    return false;
   }
  seed = seed.split(" ");
  console.log(seed);
    for(var i=0; i<3; i++){
      //console.log(seed[i]);
      if(!seed[i]){
        foo("Wrong Format!! Example:");
        return false;
      }
      seed[i] = seed[i].toUpperCase();
    }
    var url = "http://www.xe.com/currencyconverter/convert/?Amount=" + seed[2] + "&From=" + seed[0] + "&To=" + seed[1];
    //console.log(url);
    window.location.href = url;
}
foo();
//foo();
````

* **minified readable**
````javascript
function c(a) {
  a = void 0 === a ? !1 : a;
  a = prompt(a || "USD QAR 10.09", a ? "USD QAR 10.09" : "");
  if (null == a) {
    throw Error("Cancelled!");
  }
  if ("" == a) {
    c("No Data!! Example:");
  } else {
    a = a.split(" ");
    console.log(a);
    for (var b = 0; 3 > b; b++) {
      if (!a[b]) {
        c("Wrong Format!! Example:");
        return;
      }
      a[b] = a[b].toUpperCase();
    }
    window.location.href = "http://www.xe.com/currencyconverter/convert/?Amount=" + a[2] + "&From=" + a[0] + "&To=" + a[1];
  }
}
c();
````

* **minified**
````javascript
function c(a){a=void 0===a?!1:a;a=prompt(a||"USD QAR 10.09",a?"USD QAR 10.09":"");if(null==a)throw Error("Cancelled!");if(""==a)c("No Data!! Example:");else{a=a.split(" ");console.log(a);for(var b=0;3>b;b++){if(!a[b]){c("Wrong Format!! Example:");return}a[b]=a[b].toUpperCase()}window.location.href="http://www.xe.com/currencyconverter/convert/?Amount="+a[2]+"&From="+a[0]+"&To="+a[1]}}c();
````

* **bookmarkleted**
````javascript
javascript:(function()%7Bfunction%20c(a)%7Ba%3Dvoid%200%3D%3D%3Da%3F!1%3Aa%3Ba%3Dprompt(a%7C%7C%22USD%20QAR%2010.09%22%2Ca%3F%22USD%20QAR%2010.09%22%3A%22%22)%3Bif(null%3D%3Da)throw%20Error(%22Cancelled!%22)%3Bif(%22%22%3D%3Da)c(%22No%20Data!!%20Example%3A%22)%3Belse%7Ba%3Da.split(%22%20%22)%3Bconsole.log(a)%3Bfor(var%20b%3D0%3B3%3Eb%3Bb%2B%2B)%7Bif(!a%5Bb%5D)%7Bc(%22Wrong%20Format!!%20Example%3A%22)%3Breturn%7Da%5Bb%5D%3Da%5Bb%5D.toUpperCase()%7Dwindow.location.href%3D%22http%3A%2F%2Fwww.xe.com%2Fcurrencyconverter%2Fconvert%2F%3FAmount%3D%22%2Ba%5B2%5D%2B%22%26From%3D%22%2Ba%5B0%5D%2B%22%26To%3D%22%2Ba%5B1%5D%7D%7Dc()%7D)()
````

* **JSBIN:** 
https://jsbin.com/rugexal/edit?html,js,console,output

#### Enable Paste in Chrome v1

* **Name:** EnablePastev1

* **readable**
````javascript
var allowPaste = function(e){
  e.stopImmediatePropagation();
  return true;
};
document.addEventListener('paste', allowPaste, true);
````

* **minified readable**
````javascript
document.addEventListener("paste", function(a) {
  a.stopImmediatePropagation();
  return !0;
}, !0);
````

* **minified**
````javascript
document.addEventListener("paste",function(a){a.stopImmediatePropagation();return!0},!0);
````

* **bookmarkleted**
````javascript
javascript:(function()%7Bdocument.addEventListener(%22paste%22%2Cfunction(a)%7Ba.stopImmediatePropagation()%3Breturn!0%7D%2C!0)%7D)()
````

* **JSBIN:** https://jsbin.com/woxamij/edit?js,output


---
### Template

#### TITLE 

* **Name:** `//NAME//`

* **readable**
````javascript
//PASTE HERE
````

* **minified readable**
````javascript
//PASTE HERE
````

* **minified**
````javascript
//PASTE HERE
````

* **bookmarkleted**
````javascript
//PASTE HERE
````

* **JSBIN:** 

------------------------------
_Resources:_
* JS Playground: https://jsbin.com
* Minifier:   https://closure-compiler.appspot.com/home
* BML Maker:    http://mrcoles.com/bookmarklet/
